<?php
namespace DataBundle\Component\MailBunClient;

/**
 * Class MailBun_Msg
 */
class MailBun_Msg
{
    public $service;
    public $to;
    public $from;
    public $fromName;
    public $subject;
    public $bodyHtml;
    public $bodyText;
    public $bcc;
    public $attachments = array();
    public $testMode = 0;

    /**
     * @param $to
     * @param $from
     * @param $subject
     * @param $body
     * @param string $fromName
     * @param null $bcc
     * @param array $attachments
     * @param bool $isHtml
     */
    public function __construct($to, $from, $subject, $body, $fromName = '', $bcc = null, $attachments = array(), $isHtml = false)
    {
        $this
            ->addTo($to)
            ->setFrom($from, $fromName)
            ->setSubject($subject)
            ->setBcc($bcc);

        if(is_array($body)) {
            if(isset($body['text'])) {
                $this->setBody($body['text'], false);
            }
            if(isset($body['html'])) {
                $this->setBody($body['html'], true);
            }
        }
        else {
            $this->setBody($body, $isHtml);
        }

        if(is_array($attachments)) {
            foreach($attachments as $n => $a) {
                $name = strlen($n) < 2 ? 'file_' . $n : $n;
                if(file_exists($a)) {
                    $this->addAttachment($name, $a);
                }
            }
        }
    }

    /**
     * @param $attachName
     * @param $attachFilePath
     * @return $this
     */
    public function addAttachment($attachName, $attachFilePath)
    {
        $this->attachments[$attachName] = $attachFilePath;

        return $this;
    }

    /**
     * @param $service
     * @return $this
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @param $to
     * @return $this
     */
    public function addTo($to)
    {
        $this->to .= ($this->to ? ', '. $to : $to);

        return $this;
    }

    /**
     * @param $from
     * @param string $name
     * @return $this
     */
    public function setFrom($from, $name = '')
    {
        $this->from = $from;
        $this->fromName = $name;

        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @param $body
     * @param bool $html
     * @return $this
     */
    public function setBody($body, $html = false)
    {
        if($html) {
            $this->bodyHtml = $body;
        }
        else {
            $this->bodyText = $body;
        }

        return $this;
    }

    /**
     * @param $bcc
     * @return $this
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }
}