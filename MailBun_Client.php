<?php
namespace DataBundle\Component\MailBunClient;

/**
 * Class MailBun_Client
 */
class MailBun_Client
{
    /** @var resource */
    protected $curl;
    /** @var string */
    protected $username;
    /** @var string */
    protected $password;
    /** @var string */
    public $serviceUrl = 'http://mailbun.com';
    /** @var string */
    public $debugLog = '';
    /** @var string */
    public $errors = '';

    /**
     * @param string $username
     * @param string $password
     * @param string $serviceUrl
     */
    public function __construct($username, $password, $serviceUrl = '')
    {
        $this->username = $username;
        $this->password = $password;
        if($serviceUrl) {
            $this->serviceUrl = $serviceUrl;
        }
    }

    /**
     * @return resource
     */
    protected function getCurl()
    {
        if($this->curl === null) {
            $this->curl = curl_init($this->serviceUrl);
            $headers = array(
                'Accept: application/json',
            );
            $opts = array(
                CURLOPT_USERPWD => $this->username .':'. $this->password,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_HTTPHEADER => $headers,
            );
            @curl_setopt_array($this->curl, $opts);
        }

        return $this->curl;
    }

    /**
     * @param MailBun_Msg $msg
     * @return array|bool
     */
    public function send(MailBun_Msg $msg)
    {
        $this->debugLog = '';
        $this->errors = '';
        $data = $this->createDataFromMsg($msg);

        $response = $this->sendPost($data, 'send');

        return $this->parseSendResponse($response);
    }

    /**
     * @param $response
     * @return array|bool
     */
    protected function parseSendResponse($response)
    {
        $result = json_decode($response, 1);
        if($result) {
            if(isset($result['ok']) && $result['ok']) {
                $ids = array();
                if(isset($result['mails']) && is_array($result['mails'])) {
                    foreach($result['mails'] as $m) {
                        if(isset($m['id'])) {
                            $ids[] = (int)$m['id'];
                        }
                    }

                    return $ids;
                }

                $this->errors .= 'Unable to find mails info in response: '. print_r($result, 1);
                return false;
            }
            elseif(isset($result['ok'])) {
                $this->errors .= isset($result['error']) ? $result['error'] : 'unknown error: '. print_r($result, 1);
                return false;
            }
            else {
                $this->errors .= 'Unknown response format: '. print_r($result, 1);
                return false;
            }
        }
        else {
            $this->errors .= 'Unable to parse json response: '. substr($response, 0, 600);
            return false;
        }
    }

    /**
     * @param $data
     * @param $addUrl
     * @return string|bool
     */
    protected function sendPost($data, $addUrl = '')
    {
        $this->debugLog .= 'Sending POST: '. $this->serviceUrl .' '. http_build_query($data);
        $curl = $this->getCurl();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_URL, $this->serviceUrl .'/' . $addUrl);
        @curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($curl);
        $this->debugLog .= PHP_EOL .'Response: '. substr($response, 0, 600);
        $error = curl_error($curl);
        if(!$error) {
            return $response;
        }
        else {
            $this->errors .= $error;
            return false;
        }
    }

    /**
     * @param MailBun_Msg $msg
     * @return array
     */
    protected function createDataFromMsg(MailBun_Msg $msg)
    {
        $data = array(
            'to' => $msg->to,
            'from' => $msg->from,
            'from_name' => $msg->fromName,
            'subject' => $msg->subject,
            'text' => ($msg->bodyText) ? $msg->bodyText : strip_tags($msg->bodyHtml),
            'html' => $msg->bodyHtml,
            'bcc' => $msg->bcc,
            'service' => $msg->service,
            'test' => $msg->testMode,
        );
        if(is_array($msg->attachments)) {
            $i = 0;
            foreach($msg->attachments as $n => $a) {
                $data['attachments['.$i.']'] = $n;
                $af = curl_file_create($a);
                $data[$n] = $af;
                $i++;
            }
        }

        return $data;
    }
}